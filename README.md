# Inove Dados
Para que os jobs dos pipelines funcionem estamos rodando nossos próprios gitlab-runners, eles estão configurados para utilizar a image docekr:dind

## GitLab Variables
### build-image-and-push.yaml
É necessário que você crie as seguintes variáveis de forma global ou no nível do projeto:
- DOCKER_REGISTRY
- HARBOR_PROJECT

### deploy.yaml
- DOCKER_REGISTRY
- SSH_PRIVATE_KEY
- SSH_PUBLIC_KEY
- GITLAB_DELIVERY_REPO
- GITLAB_DELIVERY_REPO_NAME
- DEPLOYMENT_FILE
